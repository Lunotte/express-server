// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var morgan      = require('morgan');
var mongoose   = require('mongoose');

var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('./config'); // get our config file



var User = require('./models/user');
var dataUsers = require('./routes/users')(app);
require('./routes/categories')(app);


// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));

app.set('superSecret', config.secret); // secret variable

// configure app to use bodyParser()
// this will let us get the data from a POST
/*app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());*/

var port = process.env.PORT || 9000;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    //console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:9000/api)
router.get('/', function(req, res) {
    res.send({ message: 'hooray! welcome to our api!' });   
});


	
// more routes for our API will happen here

/*-----------AUTHENTIFICATION-------------*/
router.post('/authenticate', function(req, res) {

  // find the user
  User.findOne({
    userName: req.body.name
  }, function(err, user) {

    if (err) throw err;

    if (!user) {
      res.json({ success: false, message: 'Authentication failed. User not found.' });
    } else if (user) {

      // check if password matches
      if (user.userPassword != req.body.password) {
        res.json({ success: false, message: 'Authentication failed. Wrong password.' });
      } else {

        // if user is found and password is right
        // create a token
        var token = jwt.sign(user, app.get('superSecret'), {
          expiresIn : 60*60*24 // expires in 24 hours
        });

        // return the information including token as JSON
        res.json({
          success: true,
          message: 'Enjoy your token!',
          token: token
        });
      }   

    }

  });
});

// route middleware to verify a token
router.use(function(req, res, next) {

  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  // decode token
  if (token) {

    // verifies secret and checks exp
    jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });    
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;    
        next();
      }
    });

  } else {

    // if there is no token
    // return an error
    return res.status(403).send({ 
        success: false, 
        message: 'No token provided.' 
    });

  }
});

/*-----------USER-------------*/
router.route('/users')
	.post(saveUser)
	.get(findAllUsers);
	
router.route('/users/:user_id')
	.get(findUserById)
	.put(updateUser)
	.delete(deleteUser);
	
/*-----------CATEGORY-------------*/
router.route('/categories')
	.post(saveCategory)
	.get(findAllCategories);
	
router.route('/categories/:category_id')
	.get(findCategoryById)
	/*.put(updateCategory)
	.delete(deleteCategory)*/;

// ----------------------------------------------------



// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

mongoose.connect('mongodb://localhost/file-storage', function(err, res) {
	if(err) {
		console.log('ERROR: connecting to Database. ' + err);
	} else {
		console.log('Connected to Database');
	}
});



// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);