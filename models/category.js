var mongoose     = require('mongoose');
var Schema       = mongoose.Schema,
Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

var CategorySchema   = new Schema({
	_id: Schema.Types.ObjectId,
	label: String,
	subset:[{
		_id: Schema.Types.ObjectId,
		label: String,
		element:[{
			_id: Schema.Types.ObjectId,
			label: String,
			path: String
		}]
	}]

});

module.exports = mongoose.model('Category', CategorySchema);