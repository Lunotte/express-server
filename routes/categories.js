module.exports = function(app) {

	var User = require('../models/user.js');
	var mongoose = require('mongoose');

	findAllCategories = function(req, res) {
        User.find({},"category", function(err, categories) {
            if (err)
                res.send(err);

            res.json(categories);
        });
    };
	
	saveCategory = function(req, res) {

        var user = new User();      // create a new instance of the Category model
        user.category.label = req.body.label;  // set the category label (comes from the request)

        // save the category and check for errors
        user.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Category created!' });
        });
    }
	
	
	// GET Manage One Category
	findCategoryById = function(req, res) {
        User.find({"category._id" : req.params.category_id},'category', function(err, category) {
            if (err)
                res.send(err);
            res.json(category);
        });
    }
	
	updateCategory = function(req, res) {

        // use our bear model to find the bear we want
        Category.findById(req.params.category_id, function(err, category) {

            if (err)
                res.send(err);

            category.label = req.body.name;  // update the category info

            // save the bear
            category.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'Category updated!' });
            });

        });
    }
	
	deleteCategory = function(req, res) {
        Category.remove({
            _id: req.params.category_id
        }, function(err, category) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    }
}