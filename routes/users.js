module.exports = function(app) {

	var User = require('../models/user.js');

	var mongoose = require('mongoose');
	

	//GET - Return all tvshows in the DB
	findAllUsers = function(req, res) {
        User.find(function(err, users) {
            if (err)
                res.send(err);

            res.json(users);
        });
    };
	
	saveUser = function(req, res) {

        var user = new User();      // create a new instance of the User model
        user.username = req.body.name;  // set the user name (comes from the request)

        // save the bear and check for errors
        user.save(function(err) {
            if (err)
				throw err;
                res.send(err);

            res.json({ message: 'User created!' });
        });
    }
	
	
	// GET Manage One User
	
	findUserById = function(req, res) {
        User.findById(req.params.user_id, function(err, user) {
            if (err)
                res.send(err);
            res.json(user);
        });
    }
	
	updateUser = function(req, res) {

        // use our bear model to find the user we want
        User.findById(req.params.user_id, function(err, user) {

            if (err)
                res.send(err);

            user.username = req.body.name;  // update the users info

            // save the bear
            user.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'User updated!' });
            });

        });
    }
	
	deleteUser = function(req, res) {
        User.remove({
            _id: req.params.user_id
        }, function(err, user) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    }
}